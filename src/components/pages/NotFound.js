import React from 'react';

const NotFound = () => {
  return (
    <>
      <h1>Not found</h1>
      <p className='lead'>the page you are looking for does not exist</p>
    </>
  );
};

export default NotFound;
